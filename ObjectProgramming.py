
#dwie przestrzenie nazw
#to jest przestrzen w ktorej definiujemy nasza klase
#wszystko pod class jest wspolne dla wszystkich obiektow
class Person:
    age = 999

#def metoda
    def __init__(self,name):
        self.name = name
    def setAge(self,age):
        self.age = age
    def hello(self):
        print("Hello world, "+self.name+", mam lat:"+str(self.age))

#to jest przestrzen naszej aplikacji, ktora sie bedzie uruchamiac
if __name__ == "__main__":
    person1 = Person("Magda")
    person1.hello()
    person1.setAge(33)
    person1.hello()

    person2 = Person("Patryk")
    person2.hello()
    person2.setAge(11)
    person2.hello()